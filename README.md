[![Build Status](https://travis-ci.org/Taitsu/Lecture.svg?branch=master)](https://travis-ci.org/Taitsu/Lecture)
# Lecture
情報生命解析プログラミング演習I (group work)

### Lisence
GNU GPL v3

### Install
./waf configure build

### Test
./waf configure test

### Usage
Example
```bash
./multiplicationTable 3 3
1 2 3 
2 4 6 
3 6 9 

./multiplicationTable --add 3 3
2 3 4 
3 4 5 
4 5 6 
```
